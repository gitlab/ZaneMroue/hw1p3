package edu.bu.ec504.hw1p3.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;


/* Based on:
https://www.baeldung.com/java-base64-image-string
https://mkyong.com/java/how-to-convert-file-into-an-array-of-bytes/

see also

https://www.browserling.com/tools/file-to-base64
 */
public class ConvertToBase64 {
    final private static String inputFileName ="compressed.txt"; // the name of the file to convert
    final private static String outputFileName = "compressed_renewed.txt"; // the output file name

    static String toBase64(String theInputFile) throws IOException {
        byte[] fileContent = Files.readAllBytes(Paths.get(theInputFile));
        return Base64.getEncoder().encodeToString(fileContent);
    }

    static void fromBase64(String base64Text, String theOutputFile) throws IOException {
        byte[] decodedBytes = Base64.getDecoder().decode(base64Text);
        Files.write(Paths.get(theOutputFile),decodedBytes);
    }

    /**
     * An example of how to convert the default input file into a base64 encoding.
     */
    public static void main(String[] args) throws IOException {
        String base64 = toBase64(inputFileName);
        System.out.println("Base64:\n"+base64);

        fromBase64(base64, outputFileName);
        System.out.println("Reconsitituted as "+outputFileName);
    }
}
