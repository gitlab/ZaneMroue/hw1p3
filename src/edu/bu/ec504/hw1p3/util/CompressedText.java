package edu.bu.ec504.hw1p3.util;

import java.io.Serializable;
import java.util.List;

/**
 * An encapsulation of a list of Atoms.
 */
public final class CompressedText implements Serializable {
    public CompressedText(List<Atom> theData) {
        data = theData;
    }

    public List<Atom> getList() {
        return data;
    }

    private final List<Atom> data;
    private static final long serialVersionUID = 3L; // for serialization
}
