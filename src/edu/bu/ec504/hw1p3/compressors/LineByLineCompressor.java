package edu.bu.ec504.hw1p3.compressors;

import edu.bu.ec504.hw1p3.util.Atom;
import edu.bu.ec504.hw1p3.util.CompressedText;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * A slightly more complicated example of a compressor.
 */
public class LineByLineCompressor extends Compressor {
    /**
     * @inheritDoc
     */
    @Override
    public CompressedText processPlainText(BufferedReader in) {
        ArrayList<String> dict = emptyDict;
        ArrayList<Atom> result = new ArrayList<>();

        try {
            for (String line = in.readLine(); line!= null; line = in.readLine()) {
                // find a prefix in the dictionary
                Integer matchingLine=null;        // the last line where a matching prefix was found
                for (int ii=0; ii<dict.size(); ii++)
                    if (line.startsWith(dict.get(ii))) // i.e. dict[ii] is a prefix of line
                        matchingLine=ii;
                if (matchingLine!=null)
                    result.add(new Atom(
                            matchingLine,                                   // add the atom referencing the matching prefix
                            line.substring(dict.get(matchingLine).length()) // followed by everything past the matching prefix
                    ));
                else  // no prefix found, just throw in the entire atom, referencing the empty string
                    result.add( new Atom(0, line));

                dict.add(line);                          // add to the dictionary; the entries have to match the uncompressor's inferred dictionary entries
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new CompressedText(result);               // encapsulate the result
    }
}
